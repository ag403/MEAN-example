# MEAN-example
This is a cloneable tutorial that will also walk you through how to set up a MEAN web stack on Duke OIT virtual machines.

## NOTE:
This was "forked" from GitHub repo found [here](https://github.com/michaelcheng429/meanstacktutorial).
The codebase is not my own! I have only tweaked it to work on Duke OIT VMs.
I can't seem to figure out (or bother to spend the time) how to properly fork this from GitHub to GitLab.

## Get your VM
Request a VM from Duke OIT and provision it with the MEAN. You only get one per NetID. This can be done [here](https://vm-manage.oit.duke.edu/).
Shortly (5 minutes or so), you should get an email titled "Your VM is ready" which will give you the URL and login credentials.
From a SSH or PuTTY terminal, connect to your VM. Note: the username is `bitnami`.

## Clone this repository
From your VM terminal, run:
`git clone http://gitlab.oit.duke.edu/ag403/MEAN-example.git`

## Edit the VM firewall rules
By default, most ports are closed down on the VM. Run this command to open up port 3000:
`ufw allow 3000`

## Install, run and test the app
Navigate to the `MEAN-example` directory and run:
```
npm install
```
then
```
npm start > server.log &
```
`> sever.log` just pipes all output to a file called `server.log` and the `&` tells the terminal to fork this process so that we can continue to use the shell while the server runs.

Now, from a web browser, navigate to `hostname:3000` where `hostname` is the URL of your VM.
My web server, for example, can be accessed from:
`http://colab-sbx-110.oit.duke.edu:3000/`
You should see the example web app running! Try adding a contact from the web browser and observer the console output of your VM.
To confirm this is hitting our database, open the server terminal again and run:
`mongo admin --username root --password bitnami`
which should open a mongo shell. Within this shell, run:
```
use admin
db.contactlist.find()
```
which will return all documents in the contactlist collection, which should include any contacts you have just added.

From the web browser, try removing or editting a contact and re-run `db.contactlist.find()` from the mongo shell to confirm those changes saved to the database.

Finally, when you want to stop your web server, run `ps -au | grep node` to find the `pid` of your node process. Then `kill xx` where `xx` is the pid you found.

# MEAN Stack RESTful API Tutorial - Contact List App
<h2>MEAN Stack RESTful API Tutorial - Contact List App</h2>

<img src="http://i288.photobucket.com/albums/ll175/michaelcheng429/meanstacktutorial_zpsu72ixs47.png" alt="mean stack tutorial app">

This repo contains the code for a RESTful API Contact List App that was built using the MEAN stack:

<ul>
<li>MongoDB</li>
<li>Express</li>
<li>AngularJS</li>
<li>NodeJS</li>
</ul>

<h3>YouTube Tutorial</h3>

Here is the 5-part YouTube tutorial for this MEAN stack app:

<h4><a href="https://www.youtube.com/watch?v=kHV7gOHvNdk">Part 1/5: Using MongoDB, Express, AngularJS, and NodeJS Together</a></h4>

<h4><a href="https://www.youtube.com/watch?v=7F1nLajs4Eo">Part 2/5: AngularJS and NodeJS App Tutorial</a></h4>

<h4><a href="https://www.youtube.com/watch?v=oVIeMfvgTz8">Part 3/5: MongoDB Tutorial: Downloading, Installing, and Basics</a></h4>

<h4><a href="https://www.youtube.com/watch?v=iFsYJG3fGro">Part 4/5: RESTful API, GET and POST Data</a></h4>

<h4><a href="https://www.youtube.com/watch?v=06_SIzYXgqQ">Part 5/5: RESTful API Tutorial, DELETE and PUT Data</a></h4>

<h3>Instructions</h3>

If you have trouble with the tutorial (especially since I had to modify the code due to an AngularJS update), simply clone this repo using 

    git clone git@github.com:michaelcheng429/meanstacktutorial.git

then install the Node modules with

    npm install

then make sure MongoDB is running with

    mongod

from your MongoDB directory, and then run the code with 

    node server

You might see a message that says, 

    Error: Module did not self-register.]
    js-bson: Failed to load c++ bson extension, using pure JS version
    
Don't worry about this; the code will still work.

<h3>Have fun!</h3>

If you have any questions, feel free to leave a comment and I will try to help if I can!
